# imx6ull

#### 介绍
这里是正点原子imx6ull光盘资料中的源码与文档。因容量与大小的限制，这里只能放源码及文档。其他需要更新的资料请到http://www.openedv.com/docs/index.html 正点原子资料下载中心下载。


开发板介绍 
开发板资料介绍视频 B站哔哩哔哩链接： https://www.bilibili.com/video/av68994676

开发板QT5演示视频 B站哔哩哔哩链接： https://www.bilibili.com/video/av61255737

开发板QT5新版本超流畅桌面演示：https://www.bilibili.com/video/BV1M54y1677N

资料下载链接 
资料盘链接 
资料盘 开发板资料链接： https://pan.baidu.com/s/1inZtndgN-L3aVfoch2-sKA 提取码：m65i

资料盘 PDF合集资料链接： https://pan.baidu.com/s/1FSJY3PdFgV2WV4lT6Ps8fA 提取码：qsxb

资料盘 视频PPT笔记链接： https://pan.baidu.com/s/12C3yhpVuugtaHkhWjxfsSg 提取码：ho0m

视频网盘链接 
配套 Linux之Ubuntu入门篇 视频链接： https://pan.baidu.com/s/1GNdsA6lhPy15vahOYMMngw 提取码：80vf

配套 Linux之ARM裸机篇 视频链接： https://pan.baidu.com/s/1TjaQSuRZK0OiUCqc6S0SiQ 提取码：r27n

配套 Linux之系统移植和文件系统构建篇 视频链接： https://pan.baidu.com/s/1ZlhaCTsdBlYdSAWVtQH_sw 提取码：x2z8

配套 Linux之驱动开发篇 视频链接： https://pan.baidu.com/s/1JU95JHG-v7MKvkvXsMNhdw 提取码：n3ju

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

 初学者：
 应根据【正点原子】I.MX6U嵌入式Linux驱动开发指南V1.x.pdf搭建开发环境，可学习裸机，移植uboot、内核。编写驱动等。

 验证板子硬件：
 请参考【正点原子】I.MX6U用户快速体验V1.x.pdf。包括系统烧写，板子启动及上手操作（用户只需要有一点基础即可）。包括测试板子的性能及板子的资源使用方法。可学习到板子
 各种资源的用法。

 Qt开发者： 
 请参考【正点原子】I.MX6U 出厂系统Qt交叉编译环境搭建V1.4.pdf直接在正点原子出厂系统上运行用户编写的Qt应用程序。或者可根据【正点原子】I.MX6U 移植Qt4.8.4 V1.0.pdf 
 、【正点原子】I.MX6U 移植Qt5.12.9 V1.0.pdf学习移植Qt。

 Yocto：
 请【正点原子】I.MX6U 构建Yocto根文件系统V1.x.pdf 。但是不建议新手操作，构建Yocto，需要虚拟机硬件剩余120G以上容量。内存要大，至少分配10G以上内存给虚拟机。由于 
 Yocto项目需要连接国外网站，下载速度及编译时间等等都是一般人不能接受的！如果一个初学者有配置高的电脑，假若没有编译和下载出错，岂码至少要搞一个星期。

 快速上手项目者：
 这里指的是用经验的用户，建议参考【正点原子】I.MX6U用户快速体验V1.x.pdf，了解板子的资源（软件硬件），需要修改出厂系统的Logo请参考【正点原子】I.MX6U 修改开机进度条参考手册V1.x.pdf；需要分步更新出厂系统的固件请参考正点原子】I.MX6U 开发板文件拷贝及固件更新参考手册V1.x.pdf。

 Debian文件系统：
 需要使用Debian文件系统，请参考【正点原子】I.MX6U 移植Debian文件系统参考手册V1.x.pdf。但是这个Debian文件系统只能体验功能。一般产品都不会使用这个系统。用户可能会使用buildroot/busybox/yocto定制自己的根文件系统。

 OpenCV移植：
 请参考【正点原子】I.MX6U 移植OpenCV V1.x.pdf,目前只讲移植，关于如何使用请自行查找资料。
 
 遇到错误不会解决：
 请先在【正点原子】I.MX6U 常见问题汇总V1.1.pdf使用Ctrl + F用关键字查找。
 
 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
【正点原子】I.MX6U 移植Qt5.12.9 V1.0.pdf

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
